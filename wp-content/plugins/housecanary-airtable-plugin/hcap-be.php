<?php

// prevent direct access
defined( 'ABSPATH' ) or die( 'No ><!' );

// grab the crypto libs
use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;
use Defuse\Crypto\Encoding;

/**
 * The main controller for the backend of the plugin
 */
class HCAP_BE{
	/**
	 * [init entry point]
	 */
	public static function init(){
		// use admin_menu hook to register the options page
		add_action( 'admin_menu', 'HCAP_BE::add_options_page' );
		// handle ajax responses from backend gui
		add_action( 'wp_ajax_hcap_apikey', 'HCAP_BE::apikey_ajax_handler' );

		// enqueue backend screeeepts
		add_action( 'admin_enqueue_scripts', 'HCAP_BE::enqueue_admin_scripts' );
	}
	/**
	 * [add_message standardize response messages]
	 * @param int $status  1, 2 or 3 (error, warning, success)
	 * @param string $message the message to append
	 */
	public static function add_message($status, $message){
		return json_encode(array(
			'status' => $status,
			'message' => $message
		));
	}
	/**
	 * [add_options_page registers the options page]
	 */
	public static function add_options_page(){
		// manage_options is, by default, an admin only cap
		add_options_page( 'Airtable Plugin Settings', 'Airtable Plugin Settings', 'manage_options', 'hcap-settings', 'HCAP_BE::render_options_page' );
	}
	/**
	 * [enqueue_admin_scripts renders the backend style/script on the plugin settings page only]
	 * @param  string $hook the name of the plugin page
	 */
	public static function enqueue_admin_scripts($hook){
		// only enqueue on plugin options page
		if( $hook === 'settings_page_hcap-settings' ){
			wp_enqueue_script( 'hcap-be', HCAP_URL . '/dist/hcap-be.js', null, '1.0.0', false );
			wp_enqueue_style( 'hcap-be', HCAP_URL . '/dist/hcap-be.css', null, '1.0.0', false );
		}
	}
	/**
	 * [apikey_ajax_handler handles the request to store/update the api key]
	 */
	public static function apikey_ajax_handler(){
		// validate nonce
		if( wp_verify_nonce( $_POST['nonce'], 'hcap_apikey' ) ){
			// filter the apikey
			$_POST['key'] = filter_var($_POST['key'], FILTER_SANITIZE_STRING);
			// handle empty key
			if( empty( $_POST['key'] ) ){
				echo HCAP_BE::add_message(1, 'Key cannot be empty');
			}
			// key isn't empty
			else{
				// begin curl
				$curl = curl_init();
				// set curl options
				curl_setopt_array($curl, array(
					// url gets only 1 result to test if key works
					CURLOPT_URL => "https://api.airtable.com/v0/appaqyA7xZtjRuuQB/Data%20Points%20available%20with%20subscription?maxRecords=1",
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "GET",
					CURLOPT_HTTPHEADER => array(
						// token based auth
						"Authorization: Bearer " . $_POST['key'],
						"cache-control: no-cache"
					),
				));
				// get curl response & errors
				$response = curl_exec($curl);
				$err = curl_error($curl);
				// close curl connection
				curl_close($curl);
				// did we get error from curl
				if( $err ){
					// poop out errors
					error_log(var_export($err, true));
					echo HCAP_BE::add_message(1, 'Curl Error. Check your connection and try again.');
				}
				// no curl error
				else{
					// try to decode json from api
					$response = json_decode($response);
					// json_decode returns NULL if it can't decode
					if( $response === NULL ){
						echo HCAP_BE::add_message(1, 'Could not decode response');
					}
					// got an error from the api?
					elseif( isset($response->error) ){
						echo HCAP_BE::add_message(1, '<b>Error from Airtable API 👇</b>. Make sure you enter a valid key.<br/>' . $response->error->type . ' : ' . $response->error->message);
					}
					// no errors from curl or api
					else{
						// use the salt to generate cypher from key
						$cypher = Crypto::encrypt($_POST['key'], HCAP_LoadEncryptionKeyFromConfig());
						// try to update the option (don't autoload)
						if( $res = update_option( 'hcap_apikey', $cypher, '', 'no' ) ){
							echo HCAP_BE::add_message(3, 'Key stored');
						}
						// handle couldn't update the option
						else{
							error_log(var_export($res, true));
							echo HCAP_BE::add_message(1, 'Key could not be stored. Contact sysadmin.');
						}
					}
				}
			}
		}
		// nonce invalid... maybe block ip?
		else{
			echo HCAP_BE::add_message(1, 'Invalid Nonce');
		}
		wp_die();
	}
	/**
	 * [render_options_page renders the html for the plugin settings page]
	 */
	public static function render_options_page(){
		// grab the encrypted key from the options page
		$api_key = get_option( 'hcap_apikey', '' );
		// key isn't empty
		if( !empty($api_key) ){
			// decrypt key with salt
			$api_key = Crypto::decrypt($api_key, HCAP_LoadEncryptionKeyFromConfig());
		}
		// key's empty
		else{
			$api_key = '';
		}
		// begin output buffer
		ob_start();
		?>
		<div class="wrap">
			<div class="hcap">
				<h1 class="hcap-title">Airtable API Key</h1>
				<div class="hcap-messages">
					
				</div>
				<form action="" method="post" class="hcap-form" data-state="<?php echo empty($api_key) ? 'notsaved' : 'saved' ?>">
					<input class="hcap-form-nonce" type="hidden" name="nonce" value="<?php echo wp_create_nonce('hcap_apikey'); ?>">
					<input class="hcap-form-input" type="password" required="required" name="hcap_apikey" <?php echo !empty($api_key) ? 'value="************"' : '' ?>>
					<?php if( !empty($api_key) ): ?>
						<input class="hcap-form-button hcap-form-button--update button button-primary" type="submit" name="submit" class="button button-primary" value="Update Key">
					<?php else: ?>
						<input class="hcap-form-button hcap-form-button--save button button-primary" type="submit" name="submit" class="button button-primary" value="Save Key">
					<?php endif; ?>
				</form>
				<div class="hcap-info" style="margin-top: 40px;	">
					<h1>How to Use:</h1>
					<div>
						This plugin leverages shortcodes. To learn about how to use them visit <a href="https://codex.wordpress.org/Shortcode">https://codex.wordpress.org/Shortcode</a>. There are <?php echo count(HCAP_FE::$tables); ?> shortcodes you can use provided by this plugin. Each will express a table representing the data coming from a particular airtable. Each also takes an optional parameter <code>disable_ui</code>, which hides the search/sort/filter UI. It can be used like <code>[hcap_b disable_ui="true"]</code>, which would disable the ui on the brokerage table.<br/>
						<?php foreach( HCAP_FE::$tables as $table ): ?>
							<h4><?php echo $table->table_name; ?> Shortcode</h4>
							<code>[<?php echo $table->shortcode_name ?>]</code>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
		<?php
		// poop the shit
		echo ob_get_clean();
	}
}

HCAP_BE::init();