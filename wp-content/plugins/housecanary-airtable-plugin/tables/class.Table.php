<?php
// prevent direct access
defined( 'ABSPATH' ) or die( 'No ><!' );

class Table{
	/**
	 * [$url the url to the airtable eg: "https://api.airtable.com/v0/appaqyA7xZtjRuuQB/Data%20Points%20available%20with%20subscription"]
	 * @var string
	 */
	public $url = '';
	/**
	 * [$fields multidimentional array defining fields and their defaults and other props
	 *
	 * $fields = array(
	 * 		'column_name' => array(
	 * 			'hide_column' => true,
	 * 			'default_value' => 'piggy',
	 * 		),
	 *   	'column_name2' => array(
	 *   		'hide_column' => false,
	 *   		'default_value' => '&#10004;' // checkmark
	 *   	),
	 * );
	 * 
	 * 
	 * ]
	 * @var array
	 */
	public $fields = array();
	/**
	 * [$field_default define the default props for a field]
	 * @var array
	 */
	private $field_default = array(
		// default value
		'default_value' => '',
		// hide column on table
		'hide_column' => false,
		// config for the UI
		'ui_settings' => array(
			// the kind of UI to use for the field
			// select, select_multiple, pills, one_to_many_select
			'type' => '',
			// set this to child or parent depending on the relationship
			// type must be set to one_to_many_select for the below setting to be relevant
			'one_to_many_relationship' => '',
			// optional header for one to many filter. defaults to column name. must have type set to one_to_many_select
			'one_to_many_header' => '',
		),
		// whether column is sortable via arrow in column header
		'is_sortable' => false,
		// is this the default sort for the table
		'default_sort' => false,
	);
	/**
	 * [$shortcode_name the name to give the shortcode]
	 * @var string
	 */
	public $shortcode_name = '';

	/**
	 * [$table_name the name of the table. used in dynamically generating shortcode names/docs in backend UI page. eg: Data Points]
	 * @var string
	 */
	public $table_name = '';

	public function __construct(){
		// merge fields with the default config for a field
		foreach( $this->fields as $index => $field ){
			$this->fields[$index] = array_merge($this->field_default, $field);
		}
	}
	/**
	 * [before_records_loop fuck w the response before the main loop building table]
	 * @param  object &$response the response from the airtable api
	 */
	public function before_records_loop(&$response){

	}
	/**
	 * [custom_row_class optionally customize the table's row class]
	 * @param  int     $index  the index of the row
	 * @param  object  $record the table row
	 * @return string          the custom class
	 */
	public function custom_row_class($index, $record){
		return '';
	}

	/**
	 * [custom_table_css optionally add custom css to the table]
	 * @param  object $table    the instance of the table class
	 * @param  object $response the response from the airtable api
	 * @return string           the css
	 */
	public function custom_table_css($table, $response){
		return '';
	}
	/**
	 * [custom_table_js optionally add custom js to the table]
	 * @param  object $table    the instance of the table class
	 * @param  object $response the response from the airtable api
	 * @return string           the js
	 */
	public function custom_table_js($table, $response){
		return '';
	}
	/**
	 * [custom_table_sort optionally provide custom default sorting when rendering the html of the table in php]
	 * @param  string $a        the a compare item
	 * @param  string $b        the b compare item
	 * @return string           -1, 1, 0 or an empty string
	 */
	public function custom_table_sort($a, $b){
		return '';
	}
	/**
	 * [custom_otmgroup_sort allows custom resorting of the otmgroup terms_array var]
	 * @param  array $terms_array the terms with the parent category name as the key and the child term names as the values
	 * @return mixed              an empty value or the array. if empty then the value returned from this method is not used.
	 */
	public function custom_otmgroup_sort($terms_array){
		return '';
	}
	/**
	 * [custom_pills_sort allows for custom sorting of the pills in a pill select field]
	 * @param  array $pills an array of strings representing the pill names
	 * @return mixed        an empty string will not have the function run but an array of pills will
	 */
	public function custom_pills_sort($pills){
		return '';
	}
	/**
	 * [filter_single_value when building the table, allows control over the value in a cell. for now only works if it's a column that only uses a single value]
	 * @param  object $record      the row from the airtable response
	 * @param  string $field_name  the name of the field/column
	 * @param  mixed  $field_value the cell's value
	 * @return mixed               the filtered value
	 */
	public function filter_value($record, $field_name, $field_value){
		return '';
	}
	/**
	 * [filter_otm_group_header change the otmgroup group header]
	 * @param  string $name the otmgroup header
	 * @return string       the modified name as a string
	 */
	public function filter_otm_group_header($name){
		return $name;
	}
}
?>