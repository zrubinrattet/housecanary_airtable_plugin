<?php
	require_once HCAP_DIR . 'tables/class.Table.php';
	class Broker extends Table{
		public $url = 'https://api.airtable.com/v0/app7Omc4kzeOxzptj/Imported%20table';
		public $fields = array(
			'State' => array(
				// 'hide_column' => true,
				'default_sort' => true,
			),
			'Principal Broker' => array(
				'is_sortable' => true,
			),
			'Broker License Number' => array(
				'is_sortable' => true,
			),
			'Brokerage License Number' => array(
				'is_sortable' => true,
			),
			'Office Address' => array(
				// 'hide_column' => true,
			),
			'On Corpsite' => array(
				'hide_column' => true,
			),
		);
		public $shortcode_name = 'hcap_b';
		public $table_name = 'Broker';
		/**
		 * [before_records_loop fuck w the response before the main loop building table]
		 * @param  object &$response the response from the airtable api
		 */
		public function before_records_loop(&$response){
			// loop through records
			foreach( $response->records as $index => $record ){
				// remove records that are private
				if( empty($record->fields->{"On Corpsite"}) ){
					unset($response->records[$index]);
				}
			}
			// reindex records
			$response->records = array_values($response->records);
		}
	}
?>