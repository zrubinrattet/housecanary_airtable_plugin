<?php
	require_once HCAP_DIR . 'tables/class.Table.php';
	class DataPoints extends Table{
		public $url = 'https://api.airtable.com/v0/appaqyA7xZtjRuuQB/Data%20Points%20available%20with%20subscription';
		public $fields = array(
			'endpoint' => array(
				'hide_column' => true
			),
			'Data Point' => array(),
			'api-docs link' => array(
				'hide_column' => true,
			),
			'Description' => array(),
			'Geo level' => array(
				// 'is_sortable' => true,
				'default_sort' => true,
				'ui_settings' => array(
					'type' => 'pills',
				),
			),
			'Major category' => array(
				'hide_column' => true,
				'ui_settings' => array(
					'type' => 'one_to_many_select',
					'one_to_many_relationship' => 'parent',
					'one_to_many_header' => 'Categories',
				),
			),
			'Premium' => array(
				'hide_column' => true,
			),
			'Category' => array(
				'hide_column' => true,
				'ui_settings' => array(
					'type' => 'one_to_many_select',
					'one_to_many_relationship' => 'child',
				),
			),
			'Use cases' => array(
				'hide_column' => true,
			),
			'Category color' => array(
				'hide_column' => true,
			),
		);
		public $shortcode_name = 'hcap_dp';
		public $table_name = 'Data Points';

		private $pill_order = array(
			'Property',
			'Block',
			'Block group',
			'ZIP code',
			'Metrodiv',
			'MSA',
			'State'
		);

		public function custom_pills_sort($pills){
			return $this->pill_order;
		}

		public function custom_otmgroup_sort($terms_array){
			krsort($terms_array);
			return $terms_array;
		}

		public function filter_otm_group_header($name){
			$names = array(
				'Valuation' => '<span class="otmheader-header" style="color: #FDB813;">Valuation</span> <img class="otmheader-icon" src="' . HCAP_URL . '/dist/img/valuations.svg">',
				'Rental analysis' => '<span class="otmheader-header" style="color: #FE6D19;">Rental Analysis</span> <img class="otmheader-icon" src="' . HCAP_URL . '/dist/img/rental_analysis.svg">',
				'Market analysis' => '<span class="otmheader-header" style="color: #BC155C;">Market Analysis</span> <img class="otmheader-icon" src="' . HCAP_URL . '/dist/img/market_analysis.svg">',
			);
			return $names[$name];
		}

		public function before_records_loop(&$response){
			$icon_map = array(
				'Valuation' => '<img class="otmheader-icon" src="' . HCAP_URL . '/dist/img/valuations.svg">',
				'Rental analysis' => '<img class="otmheader-icon" src="' . HCAP_URL . '/dist/img/rental_analysis.svg">',
				'Market analysis' => '<img class="otmheader-icon" src="' . HCAP_URL . '/dist/img/market_analysis.svg">',
			);
			// loop through the records in the response
			foreach( $response->records as $index => $record ){
				// does the record have a valid url in the "api-docs link" field?
				if( filter_var($record->fields->{'api-docs link'}, FILTER_VALIDATE_URL) ){
					// then convert endpoint field entry to an anchor tag
					$str = $icon_map[$record->fields->{'Major category'}] . ' <a target="_blank" href="' . $record->fields->{'api-docs link'} . '">' . $record->fields->{'Data Point'} . '</a>';
					// if it's a premium row add the badge
					if( $record->fields->{'Premium'} ) $str .= '<span class="premiumbadge">Premium</span>';
					// set the datapoint field
					$record->fields->{'Data Point'} = $str;
				}
			}
		}
		public function custom_table_js($table, $response){
			ob_start();
			?>
				if( typeof window.hcap_dp.col_sorts == 'undefined' ){
					window.hcap_dp.col_sorts = {};
				}
				// -1 = a comes first
				// 1 = b comes first
				window.hcap_dp.col_sorts['Geo level'] = function(a, b){
					let order = [
									'Property',
									'Block',
									'Block group',
									'ZIP code',
									'Metrodiv',
									'MSA',
									'State'
								];
					if( navigator.userAgent.toLowerCase().indexOf('firefox') > -1 ){
						if( a[2] == 'asc' ){
							if( order.indexOf(b[1]) > order.indexOf(a[1]) ){
								return 1;
							}
							else if(order.indexOf(b[1]) == order.indexOf(a[1])){
								return 0;
							}
						}
						else{
							if( order.indexOf(a[1]) > order.indexOf(b[1]) ){
								return 1;
							}
							else if(order.indexOf(b[1]) == order.indexOf(a[1])){
								return 0;
							}
						}
					}
					else{
						if( a[2] == 'asc' ){
							if( order.indexOf(a[1]) > order.indexOf(b[1]) ){
								return -1;
							}
							else if(order.indexOf(b[1]) == order.indexOf(a[1])){
								return 0;
							}
						}
						else{
							if( order.indexOf(b[1]) > order.indexOf(a[1]) ){
								return -1;
							}
							else if(order.indexOf(b[1]) == order.indexOf(a[1])){
								return 0;
							}
						}
					}
				};
			<?php
			return ob_get_clean();
		}
		public function custom_table_sort($a, $b){
			if( array_search($b, $this->pill_order) > array_search($a, $this->pill_order) ){
				return -1;
			}
			else{
				return 1;
			}
		}
		public function custom_table_css($table, $response){
			$output = '
				#hcap_dp .premiumbadge{
					background-color: #00D94B;
					border-radius: 20px;
					padding: 1px 12px;
					color: white;
					font-size: 12px;
					margin-left: 25px;
				}
				#hcap_dp .colorbadge{
					border-radius: 18px;
					width: 15px;
					height: 15px;
					display: inline-block;
					margin-right: 10px;
					position: relative;
					top: 2px;
				}
			';

			return $output;
		}
	}
?>