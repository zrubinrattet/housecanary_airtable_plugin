import $ from 'jquery';

/**
 * [HCAP controls the backend UI]
 * @type {Object}
 */
let HCAP = {
	/**
	 * [_init entry point]
	 */
	_init : () => {
		$(document).ready(HCAP._ready);
	},
	/**
	 * [_ready fires when document loaded]
	 */
	_ready : () => {
		// init vars
		HCAP.updateButton = $('.hcap-form-button--update');
		HCAP.saveButton = $('.hcap-form-button--save');
		HCAP.input = $('.hcap-form-input');
		HCAP.form = $('.hcap-form');
		HCAP.state = HCAP.form.attr('data-state');
		HCAP.messageContainer = $('.hcap-messages');

		// prevent default form submit behavior
		$('.hcap-form').on('submit', e => e.preventDefault());

		// add event listeners to buttons if they exist
		if( HCAP.updateButton.length > 0 ){
			HCAP.updateButton.on('click', HCAP._updateButtonClickHandler);
		}
		if( HCAP.saveButton.length > 0 ){
			HCAP.saveButton.on('click', HCAP._saveButtonClickHandler);
		}
	},
	/**
	 * [_addMessage adds a message to the container]
	 * @param  {mixed} status  number or string
	 * @param  {string} message the message
	 */
	_addMessage : (status, message) => {
		// convert to string if number
		if( typeof status == 'number' ){
			let statuses = {
				1 : 'error',
				2 : 'warning',
				3 : 'success',
			};
			status = statuses[status];
		}
		// add new message
		$(`<div class="hcap-messages-message hcap-messages-message--${status}">${message}<i>×</i></div>`)
			// hide it
			.hide()
			// add it to the messages container
			.appendTo(HCAP.messageContainer)
			// reveal it
			.fadeIn(250);
		// listen for click events on the little x on the message
		HCAP.messageXs = $('.hcap-messages-message i').on('click', HCAP._deleteMessageHandler);
	},
	/**
	 * [_deleteMessageHandler]
	 * @param  {object} e the event object
	 */
	_deleteMessageHandler : (e) => {
		// get the active message
		let message = $(e.target).parent('.hcap-messages-message');
		// fade it out and remove it from the dom when it's done
		message.fadeOut(250, () => message.remove());
	},
	/**
	 * [_clearMessages removes all messages from the container. not using it at time of writing but maybe useful later.]
	 */
	_clearMessages : () => {
		// are there messages to remove?
		if( HCAP.messageContainer.children.length > 0 ){
			// then remove em
			HCAP.messageContainer.children.fadeOut(250, () => HCAP.messageContainer.children.remove());
		}
	},
	/**
	 * [_changeStateTo]
	 * @param  {string} state the state of the UI
	 */
	_changeStateTo : (state) => {
		// is the state type supported
		if( $.inArray(state, ['saved', 'notsaved']) !== false ){
			// change the UI based on the state
			switch(state) {
				case 'saved':
					HCAP.saveButton.remove();
					if( HCAP.updateButton.length == 0 ){
						HCAP.form.append('<input class="hcap-form-button hcap-form-button--update button button-primary" type="submit" name="submit" class="button button-primary" value="Update Key">');
						HCAP.updateButton = $('.hcap-form-button--update');
					}
					HCAP.input.attr({
						value : '***********',
					});
					break;
				case 'notsaved':
					HCAP.updateButton.remove();
					if( HCAP.saveButton.length == 0 ){
						HCAP.form.append('<input class="hcap-form-button hcap-form-button--save button button-primary" type="submit" name="submit" class="button button-primary" value="Save Key">');
						HCAP.saveButton = $('.hcap-form-button--save');
					}
					HCAP.input.removeAttr('value');
					break;
			}
			HCAP.state = state;
		}
		// throw a reference error if state was stupid
		else{
			throw new ReferenceError('state must either be "saved" or "notsaved"');
		}
	},
	/**
	 * [_updateButtonClickHandler p much same as saveButtonClickHandler but with a check to see if asterisks were submitted & error handling]
	 * @param  {object} e the event object
	 */
	_updateButtonClickHandler : (e) => {
		// begin checkin to see if all input chars are stars
		let allStars = true;
		for (var i = 0; i < HCAP.input.val().length; i++) {
			if( HCAP.input.val().charAt(i) != '*'){
				allStars = false;
			}
		}
		// they're all stars
		if( allStars ){
			// add a message
			HCAP._addMessage(2, 'Please retype your key');
		}
		// they're not all stars so proceed as usual
		else{
			HCAP._saveButtonClickHandler(e);
		}
	},
	/**
	 * [_saveButtonClickHandler tries to save the apikey]
	 * @param  {object} e the event object
	 */
	_saveButtonClickHandler : (e) => {
		// try to get auth token from airtable
		if( HCAP.input.val().length > 0 ){
			// make the post request to WP
			$.post(ajaxurl, {
				action: 'hcap_apikey',
				key: HCAP.input.val(),
				nonce: $('.hcap-form-nonce').val(),
			}, (res, status, xhr) => {
				// try to convert response to json
				try {
					res = JSON.parse(res);
					switch(res.status) {
						case 3:
							// change to key saved state
							if( HCAP.status != 'saved' ){
								HCAP._changeStateTo('saved');
								HCAP._addMessage(3, res.message);
							}
							break;
						default:
							HCAP._addMessage(res.status, res.message)
							break;
					}
				}
				// log errors if they happen
				catch(e) {
					console.log(e);
				}
			})
		}
	},
};

HCAP._init();