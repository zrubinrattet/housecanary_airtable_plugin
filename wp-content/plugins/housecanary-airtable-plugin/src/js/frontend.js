import $ from 'jquery';
import Rainbow from 'rainbowvis.js';
import {debounce, mergeArrays, intersect} from './misc.js';


/**
 * [HCAP handles the front end behavioral stuff]
 * @type {Object}
 */
let HCAP = {
	/**
	 * [Table handles the table frontend UI]
	 * @type {class}
	 */
	Table : class Table {
		constructor(table) {
			// init vars
			this.container = $(table);
			this.searchInput = $('.hcap-wrap-filters-search', this.container);
			this.sortables = $('th span.anglesort', this.container);
			this.tableRows = $('.hcap-wrap-tablecontainer tr[data-index]', this.container);
			this.tableContainer = $('.hcap-wrap-tablecontainer-table', this.container);
			this.otmgroups = $('.hcap-wrap-filters-otmgroup', this.container);
			this.pillsliders = $('.hcap-wrap-filters-pillswrap', this.container);
			this.filterToggle = $('.hcap-wrap-filters-toggle-button', this.container);

			// get the raw data from the api & other junk
			this.tableData = window[this.container.attr('id')];

			// listen for keyup on the search input
			if( this.tableRows.length > 0 ){
				this._colorRows(this);
				// use debounce because change event fires 3 times unnecessarily when removing all selected items in a field
				this.searchInput.on('keyup', debounce(this._filterTable.bind(null, this)));
			}

			// listen for sortable (arrow in column header) click
			if( this.tableRows.length > 0 && this.sortables.length > 0 ){
				this.sortables.on('click', debounce(this._filterTable.bind(null, this)));
			}
			// is there a one to many group?
			if( this.otmgroups.length > 0 ){
				// get the collective widths of all the groups in the ui to then hardcode a width to the scrolling wrapper
				let collectiveWidths = 0;
				$('.hcap-wrap-filters-otmgroup-ui-wrap-group', this.otmgroups).each( (index, el) => {
					// include margin in width calculation
					collectiveWidths += $(el).outerWidth(true);
				} );
				// assign the calculated width to the scrolling wrapper
				$('.hcap-wrap-filters-otmgroup-ui-wrap', this.otmgroups).css('width', collectiveWidths);
				// listen to select all
				$('.hcap-wrap-filters-otmgroup-selectall > div', this.otmgroups).on('click', this._otmSelectAllClickHandler.bind(null, this));
				// listen to individual filters in otmgroup
				$('.hcap-wrap-filters-otmgroup-ui-wrap-group-checkboxes-checkbox-inner input', this.otmgroups).on('click', this._filterTable.bind(null, this));
			}

			// are there pill sliders?
			if( this.pillsliders.length > 0 ){
				// build the rainbow
				let rainbow = new Rainbow();
				rainbow.setSpectrum('#4E6AE7', '#6E0296');
				rainbow.setNumberRange(0, $('.hcap-wrap-filters-pillswrap-pills-pill', this.pillsliders).length);

				// apply the rainbow as a stylesheet instead of inline styles per pill
				let styles = '<style type="text/css">';
				$('.hcap-wrap-filters-pillswrap-pills-pill', this.pillsliders).each((index, pill) => {
					styles += '.hcap-wrap-filters-pillswrap-pills-pill.active:nth-child(' + (index + 1) + '){background-color : #' + rainbow.colourAt(index) + '; border-color: #' + rainbow.colourAt(index) + ';}';
					styles += '.hcap-wrap-filters-pillswrap-pills-pill:nth-child(' + (index + 1) + '){color : #' + rainbow.colourAt(index) + '; border-color: #' + rainbow.colourAt(index) + '; background-color: transparent}';
				});
				styles += '</style>';
				$('head').append(styles)

				// listen for pill clicks
				$('.hcap-wrap-filters-pillswrap-pills-pill', this.pillsliders).on('click', this._pillClickHandler.bind(null, this));
			}

			// is there a filterToggle?
			if( this.filterToggle.length > 0 ){
				// listen for the click event on the toggle and pass a ref to the current table
				this.filterToggle.on('click', this._filterToggleHandler.bind(null, this));
				// immediately set up mobile filter toggle
				this._mobileToggleFilter(this, null);
				// listen for the resize event on the window and maybe collapse/expand the filter
				$(window).on('resize', this._mobileToggleFilter.bind(null, this));
			}
		}
		/**
		 * [_mobileToggleFilter expands/collapses the filter UI depending on toggle status & window width]
		 * @param  {object} Table the table element
		 * @param  {object} e     the event object
		 */
		_mobileToggleFilter(Table, e){
			// remove the toggle's collapsing on desktop
			if( $(window).width() >= 1168 ){
				$('.filtergroup', Table.filterToggle.parents('.hcap-wrap-filters')).removeClass('active');
				Table.filterToggle.removeClass('active');
			}
			else if( $(window).width() < 1168 && Table.filterToggle.hasClass('active') == false ){
				$('.filtergroup', Table.filterToggle.parents('.hcap-wrap-filters')).removeClass('active');
			}
		}
		/**
		 * [_filterToggleHandler shows/hides the filters on click]
		 * @param  {object} Table the table we're working with
		 * @param  {object} e     the event object
		 */
		_filterToggleHandler(Table, e){
			$('.filtergroup', Table.filterToggle.parents('.hcap-wrap-filters')).toggleClass('active');
			Table.filterToggle.toggleClass('active');
		}
		/**
		 * [_otmSelectAllClickHandler when checked all the checkboxes in the otmgroup become checked and vice versa]
		 * @param  {object} Table the table we're working with
		 * @param  {object} e     the event object
		 */
		_otmSelectAllClickHandler(Table, e){
			$('.hcap-wrap-filters-otmgroup-ui-wrap-group-checkboxes-checkbox input', $(e.target).parents('.hcap-wrap-filters-otmgroup')).prop('checked', $(e.target).hasClass('hcap-wrap-filters-otmgroup-ui-selectall-button'));
			Table._filterTable(Table, e);
		}
		/**
		 * [_pillClickHandler toggles the active class for the pill filter]
		 * @param  {object} Table the table we're working with
		 * @param  {object} e     the event object
		 */
		_pillClickHandler(Table, e){
			$(e.target).toggleClass('active');
			Table._filterTable(Table, e);
		}
		/**
		 * [_filterTable one filter func to rule them all]
		 * @param  {object} Table the table we're working with
		 * @param  {object} e the event object
		 */
		_filterTable(Table, e) {
			// build state of filters
			// set initial state
			let filterState = {};
			// capture state of search input
			filterState.search = Table.searchInput.val();

			// capture state of the otmgroups
			Table.otmgroups.each( (index, otmgroup) => {
				// loop through all the checkboxes per each otmgroup
				$(otmgroup).find('.hcap-wrap-filters-otmgroup-ui-wrap-group-checkboxes-checkbox-inner input').each( (index, input) => {
					// set the default state for the column as an empty array if it's not already defined
					if( typeof filterState[input.dataset.column] === 'undefined' ) filterState[input.dataset.column] = [];
					// if the input is checked then add it to the filterState object by column (key)
					if( input.checked ) filterState[input.dataset.column].push(input.dataset.term); 
				} );
			} );
			// capture state of the pill sliders
			Table.pillsliders.each( (index, pillslider) => {
				// loop through the pills
				$(pillslider).find('.hcap-wrap-filters-pillswrap-pills-pill').each( (index, pill) => {
					// set the default state for the column as an empty array if it's not already defined
					if( typeof filterState[pill.dataset.column] === 'undefined' ) filterState[pill.dataset.column] = [];
					// if the pill is active then add it to the filterState object by column (key)
					if( $(pill).hasClass('active') ) filterState[pill.dataset.column].push(pill.dataset.term); 
				} );
			} );

			// init the visible rows array
			let visibleRows = new Set();

			// loop over the records
			$.each(Table.tableData.records, (rowIndex, row) => {
				// store if the row should be hidden or not
				let hideRow = false;
				// let's start with search
				// we've got stuff in the search input?
				// and
				// is the search in a space separated string of all the row's values mashed together for the search input affective?
				if( filterState.search.length > 0 && Object.values(row.fields).join(' ').toLowerCase().indexOf(filterState.search.toLowerCase()) > -1 ){
					// add the row to the visible rows
					visibleRows.add(rowIndex);
				}
				// if the row can't be found
				else{
					// and there's shit in the search field then hide the row
					if( filterState.search.length > 0 ) hideRow = true;
				}

				// now for the other fields
				$.each(filterState, (key, terms) => {
					// bail on search (since we already did that)
					if( key === 'search' ) return;

					// if the active filters for the column does not contain a value in the airtable row & column, then hide the row
					if( terms.indexOf(row.fields[key]) == -1 ){
						hideRow = true;
					}

					// if we should hide the row
					if( hideRow ){
						// delete it from the visibleRows in case it got added
						visibleRows.delete(rowIndex);
					}
					else{
						// add the row if it shouldn't be hidden (doi)
						visibleRows.add(rowIndex);
					}
				} );

			} );

			// convert to array
			visibleRows = [...visibleRows];

			// typecast visible rows to numbers
			visibleRows = visibleRows.map(Number);

			// if there's a sort selected && rows are showing
			if( Table.sortables.length > 0 && $(e.target).is('.anglesort') ){
				// begin sorting the table's rows
				let sortData = [];
				// get multi-d array of index + sort column values
				$.each(Table.tableData.records, (recordIndex, record) => {
					sortData.push([recordIndex, record.fields[ $(e.target).attr('data-column') ], $(e.target).attr('data-direction')]);	
				} );
				// sort the data alphabetically
				sortData = sortData.sort( function(Table, e, a, b) {
					// if the column supports arrays
					if(a[1].constructor == Array){
						if(a[1][0] === "" || a[1][0] === null) return 1;
					    if(b[1][0] === "" || b[1][0] === null) return -1;
					    if(a[1][0] === b[1][0]) return 0;
					    return a[1][0] < b[1][0] ? -1 : 1;
					}
					// otherwise just use simple string compare
					else{
						// is there a custom sort set?
						if( typeof window[Table.container.attr('id')].col_sorts != 'undefined'
							&&
							typeof window[Table.container.attr('id')].col_sorts[$(e.target).attr('data-column')] != 'undefined'
						){
							return window[Table.container.attr('id')].col_sorts[$(e.target).attr('data-column')](a, b);
						}
						// there's no custom sort set
						else{
							if( a[2] == 'asc' ){
								return b[1].localeCompare(a[1]);
							}
							else{
								return a[1].localeCompare(b[1]);
							}
						}
					}
				}.bind(null, Table, e) );
				// loop through the reversed sortdata so append works in the correct order
				sortData.forEach( (row) => {
					// get the row & remove it
					let tablerow = $('tr[data-index=' + row[0] + ']', Table.tableContainer).remove();
					// append the row to the table container
					Table.tableContainer.append(tablerow);
				} );
				// re-store the tableRows
				Table.tableRows = $('.hcap-wrap-tablecontainer-table tr[data-index]', Table.container);
			}


			// show the visible rows
			if( visibleRows.length > 0 ){
				Table.tableRows.filter( (index, el) => {
					$(el).toggle( visibleRows.indexOf( $(el).data('index') ) > -1 );
				} );
			}
			// hide all rows if there's a search input and no visible rows
			else{
				Table.tableRows.filter( (index, el) => {
					$(el).hide();
				} );
			}

			// color the visible rows
			Table._colorRows(Table);
			
			// flip the direction based on the column header sort ui
			if( e.type == 'click' && $(e.target).hasClass('anglesort') ){
				$(e.target).attr('data-direction', $(e.target).attr('data-direction') === 'asc' ? 'desc' : 'asc');
			}
		}
		/**
		 * [_colorRows colors the rows in an alternating pattern]
		 * @param  {object} Table the table we're working with
		 */
		_colorRows(Table){
			let visibles = 0;
			Table.tableRows.each( function(i, el) {
				$(el).removeClass('rowbg');
				if( $(el).is(':visible') ){
					if( visibles % 2 != 0 ){
						$(el).addClass('rowbg');
					}
					visibles = visibles + 1;
				}
			} );
		}
	},
	/**
	 * [_init entry point]
	 */
	_init : () => {
		// do things right after .hcap element has been painted
		$.when($('.hcap')).then( (a) => {
			$('.hcap').each((index, table) => {
				new HCAP.Table(table);
			});
		} );
	},
};
// kick it off
HCAP._init();