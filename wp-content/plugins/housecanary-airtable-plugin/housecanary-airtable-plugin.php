<?php
/*
Plugin Name: House Canary Airtable Plugin
Description: Lightweight plugin that expresses token-based API-derrived airtable data, stored in memory, on frontend via shortcodes
Author: Zach Rubin-Rattet
Author URI: zrrdigitalmedia.com
*/

// prevent direct access
defined( 'ABSPATH' ) or die( 'No ><!' );
// grab the composer autoload
require_once('vendor/autoload.php');
// grab the crypto libs
use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;
use Defuse\Crypto\Encoding;

// create salt on activation
register_activation_hook(__FILE__, function(){
	delete_option( 'hcap_apikey' );
	$key = Key::createNewRandomKey();
	file_put_contents(HCAP_DIR . '/hcapsalt.txt', $key->saveToAsciiSafeString());
});

// delete salt & wp_options entry on deactivation
register_deactivation_hook( __FILE__, function(){
	if( file_exists(HCAP_DIR . '/hcapsalt.txt') ){
		unlink(HCAP_DIR . '/hcapsalt.txt');
		delete_option( 'hcap_apikey' );
	}
} );



// define constants
define('HCAP_URL', plugins_url( 'housecanary-airtable-plugin' ) );
define('HCAP_DIR', plugin_dir_path( __FILE__ ));

// load the salt
function HCAP_LoadEncryptionKeyFromConfig() {
    $keyAscii = file_get_contents( HCAP_DIR . '/hcapsalt.txt' );
    return Key::loadFromAsciiSafeString($keyAscii);
}

require_once('hcap-be.php');
require_once('hcap-fe.php');

?>