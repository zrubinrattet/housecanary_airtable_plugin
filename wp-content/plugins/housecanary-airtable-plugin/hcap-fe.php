<?php

// prevent direct access
defined( 'ABSPATH' ) or die( 'No ><!' );

// grab the crypto libs
use Defuse\Crypto\Crypto;

class HCAP_FE{
	/**
	 * [$tables stores all the tables]
	 * @var [type]
	 */
	public static $tables;
	/**
	 * [init entry point]
	 */
	public static function init(){
		// loop through the table filenames
		foreach( array_diff(scandir(HCAP_DIR . '/tables'), array('..', '.', 'class.Table.php')) as $table ){
			// require the table file
			require_once HCAP_DIR . '/tables/' . $table;
			// get the classname
			$class_name = explode('.', $table)[1];
			// dynamically create a new table instance and store it
			HCAP_FE::$tables[] = new $class_name();
		}

		// register client screeepts
		add_action( 'wp_enqueue_scripts', 'HCAP_FE::register_client_scripts' );

		// build the shortcode for each table
		foreach( HCAP_FE::$tables as $table ){
			add_shortcode( $table->shortcode_name, 'HCAP_FE::build_shortcode' );
		}
	}
	/**
	 * [register_client_scripts registers the frontend style/script to be enqueued in the shortcode callback]
	 */
	public static function register_client_scripts(){
		wp_register_script( 'hcap-fe', HCAP_URL . '/dist/hcap-fe.js', null, '1.0.0', false );
		wp_register_style( 'hcap-fe', HCAP_URL . '/dist/hcap-fe.css', null, '1.0.0', false );
	}
	/**
	 * [build_hcap_filter_ui build filtering ui from column names]
	 * @param object $response the response from the airtable api
	 * @param object $table    the table instance
	 */
	public static function build_hcap_filter_ui($response, $table){
		// init html output
		$output = '';
		// init field names
		$ui_field_names = array();
		// get field names
		foreach( $table->fields as $name => $data ){
			if( in_array($data['ui_settings']['type'], array( 'select', 'one_to_many_select', 'select_multiple', 'pills' )) ){
				$ui_field_names[] = $name;
			}
		}
		// we got field names right?
		if( !empty($ui_field_names) ){
			// loop through columns that'll get turned into fields
			foreach( $ui_field_names as $column ){


				if( $table->fields[$column]['ui_settings']['type'] == 'select' ){
					// build select field/options from col_data
					$output .= '<div class="hcap-wrap-filters-selectcontainer"><select autocomplete="off" placeholder="' . $column . '" class="hcap-wrap-filters-selectcontainer-select" name="' . $column . '"' . ( $table->fields[$column]['ui_settings']['type'] === 'select_multiple' ? ' multiple="multiple"' : '' ) . '>';
					// add empty option to single select fields
					if( $table->fields[$column]['ui_settings']['type'] !== 'select_multiple' ){
						$output .= '<option></option>';
					}
					// init column data
					$col_data = array();
					// loop through all the records and build arrays of options for each col type
					foreach( $response->records as $record ){
						if( gettype( $record->fields->{$column} ) == 'array' ){
							foreach( $record->fields->{$column} as $el ){
								$col_data[] = $el;
							}
						}
						else{
							$col_data[] = $record->fields->{$column};
						}
					}
					// dedupe/strip null values/reorder keys
					$col_data = array_values(array_unique(array_filter($col_data)));
					// add the options
					foreach( $col_data as $option ){
						$output .= '<option value="' . $option . '" data-column="' . $column . '">' . $option . '</option>';
					}
					// clear the col_data from memory
					unset($col_data);
					// close the select tag
					$output .= '</select><img src="' . HCAP_URL . '/dist/img/carat.svg" class="hcap-wrap-filters-selectcontainer-icon"/></div>';
				}
				elseif( $table->fields[$column]['ui_settings']['type'] == 'one_to_many_select' && $table->fields[$column]['ui_settings']['one_to_many_relationship'] === 'parent' ){
					// $terms_array holds an associative array of the parent terms as the keys and their child terms as the values, all alphabetized
					// eg:
					// array (
					//   'Market analysis' => 
					//   array (
					//     0 => 'Market details and risks',
					//     1 => 'Market outlook',
					//   ),
					//   'Rental analysis' => 
					//   array (
					//     0 => 'Rental market outlook',
					//     1 => 'Rental valuation and forecasts',
					//     2 => 'Rental valuation context',
					//   ),
					//   'Valuation' => 
					//   array (
					//     0 => 'Property risk',
					//     1 => 'Property forecasts',
					//     2 => 'Property valuation',
					//     3 => 'Property valuation context',
					//     4 => 'Property transaction history',
					//   ),
					// )
					$terms_array = array();

					// get the child key name
					$child_key = array_keys(array_filter($table->fields, function($field, $key){
						if( $field['ui_settings']['one_to_many_relationship'] === 'child' ) return true;
					}, ARRAY_FILTER_USE_BOTH))[0];

					// build the terms_array
					foreach( $response->records as $row ){
						if( !empty($row->fields->{$column}) ){
							$terms_array[$row->fields->{$column}][] = $row->fields->{$child_key};
						}
					}

					// dedupe, alphabetize and reindex the terms array
					foreach( $terms_array as $key => $terms ){
						sort($temp = array_unique($terms));
						$terms_array[$key] = array_values($temp);
						unset($temp);
					}

					// apply custom sorting of the terms array if it exists
					if( !empty( $terms_array_custom_sorted = $table->custom_otmgroup_sort( $terms_array ) ) ){
						$terms_array = $terms_array_custom_sorted;
					}
					else{
						// alphabetize $terms_array
						// ksort instead of sort to preserve keys
						ksort($terms_array);
					}

					$output .= '<div class="hcap-wrap-filters-otmgroup filtergroup">';

						$output .= '<h3 class="hcap-wrap-filters-otmgroup-header">' . ( empty($otmheader = $table->fields[$column]['ui_settings']['one_to_many_header']) ? $column : $otmheader) . ':</h3>';

						$output .= '<div class="hcap-wrap-filters-otmgroup-selectall checkboxgroup">';

							$output .= '<div class="hcap-wrap-filters-otmgroup-ui-selectall-button">Select all</div>';
							$output .= '<div class="hcap-wrap-filters-otmgroup-ui-selectall-clear">Clear selection</div>';

						$output .= '</div>';
						$output .= '<div class="hcap-wrap-filters-otmgroup-ui">';
							$output .= '<div class="hcap-wrap-filters-otmgroup-ui-wrap">';

								// loop through terms array
								foreach( $terms_array as $key => $terms ){

									$output .= '<div class="hcap-wrap-filters-otmgroup-ui-wrap-group">';

										$output .= '<h4 class="hcap-wrap-filters-otmgroup-ui-wrap-group-header">' . $table->filter_otm_group_header($key) . '</h4>';

										$output .= '<ul class="hcap-wrap-filters-otmgroup-ui-wrap-group-checkboxes">';

											foreach( $terms_array[$key] as $term ){

												$output .= '<li class="hcap-wrap-filters-otmgroup-ui-wrap-group-checkboxes-checkbox checkboxgroup"><div class="hcap-wrap-filters-otmgroup-ui-wrap-group-checkboxes-checkbox-inner pretty p-svg p-curve">';

													$output .= '<input autocomplete="off" data-column="' . $child_key . '" type="checkbox" data-term="' . $term . '" id="' . strtolower(str_replace(' ', '_', $term)) . '" checked/>';

													$output .= '<div class="state"><svg class="svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
																						 viewBox="0 0 222.4 170.8" style="enable-background:new 0 0 222.4 170.8;" xml:space="preserve">
																					<style type="text/css">
																						.st0{fill:#4E6AE7;}
																					</style>
																					<path class="st0" d="M78.9,170.8c-26.1-23.5-52.5-47.3-78.9-71c10.6-10.6,21.1-21,31.6-31.6c15.8,15.7,31.7,31.6,47.5,47.4
																						C117.9,76.9,156.5,38.4,194.9,0c9.4,9.4,18.5,18.5,27.5,27.6C174.8,75.1,126.9,122.8,78.9,170.8z"/>
																					</svg><label for="' . strtolower(str_replace(' ', '_', $term)) . '" data-column="' . $child_key . '">' . $term . '</label></div></div>';
												// end checkbox
												$output .= '</li>';
											}
										// end checkboxes
										$output .= '</ul>';
									// end group
									$output .= '</div>';
								}

								// remove the temp array from memory
								unset($temp);
							// end wrap
							$output .= '</div>';
						// end UI
						$output .= '</div>';
					// end otmgroup
					$output .= '</div>';
				}
				elseif( $table->fields[$column]['ui_settings']['type'] === 'pills' ){
					
					$output .= '<div class="hcap-wrap-filters-pillswrap filtergroup">';

						$output .= '<h3 class="hcap-wrap-filters-pillswrap-header">' . $column . ':</h3>';

						$output .= '<ul class="hcap-wrap-filters-pillswrap-pills">';

							$pills = array();
							foreach( $response->records as $row ) $pills[] = $row->fields->{$column};
							// dedupe & reindex keys
							$pills = array_values(array_unique($pills));
							// use custom sort if its defined
							if( !empty( $custom_pills_sort = $table->custom_pills_sort($pills) ) ){
								$pills = $custom_pills_sort;
							}
							// otherwise just sort alphabetically
							else{
								sort($pills);
							}

							foreach( $pills as $pill ){

								$output .= '<li class="hcap-wrap-filters-pillswrap-pills-pill active" data-column="' . $column . '" data-term="' . $pill . '">';

									$output .= '<span class="hcap-wrap-filters-pillswrap-pills-pill-text">' . $pill . '</span><img class="hcap-wrap-filters-pillswrap-pills-pill-remove" src="' . HCAP_URL . '/dist/img/remove.svg"/>';

								$output .= '</li>';

							}

						$output .= '</ul>';

					// end pillswrap
					$output .= '</div>';

				}
			}
		}
		return $output;
	}
	/**
	 * [build_shortcode enqueues the script/style so its only loaded once when a shortcode is rendered & builds the table for front-end rendering]
	 */
	public static function build_shortcode($atts, $content, $shortcode){

		$atts = shortcode_atts( array(
			'disable_ui' => false,
		), $atts );


		// init table
		$table = null;
		// get table by shortcode name
		foreach( HCAP_FE::$tables as $_table ) if( $_table->shortcode_name === $shortcode ) $table = $_table;
		// catch bad shortcode name
		if( $table === null ) wp_die('You must provide a shortcode_name to the Table class');

		// enqueue style/script
		wp_enqueue_script( 'hcap-fe' );
		wp_enqueue_style( 'hcap-fe' );
		// start the response
		$response = '';
		// start the output
		$output = '<div class="hcap" id="' . $shortcode . '">
					<div class="hcap-wrap">';

		// if error encountered - display it
		$error_message = '';
		// grab the encrypted key from the options page
		$api_key = get_option( 'hcap_apikey', '' );
		// key isn't empty
		if( !empty($api_key) ){
			
			// decrypt key with salt
			$api_key = Crypto::decrypt($api_key, HCAP_LoadEncryptionKeyFromConfig());

			// begin curl
			$curl = curl_init();
			// set curl options
			curl_setopt_array($curl, array(
				// url gets only 1 result to test if key works
				CURLOPT_URL => $table->url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
					// token based auth
					"Authorization: Bearer " . $api_key,
					"cache-control: no-cache"
				),
			));
			// get curl response & errors
			$response = curl_exec($curl);
			$err = curl_error($curl);
			// close curl connection
			curl_close($curl);
			// did we get error from curl
			if( $err ){
				// poop out errors
				error_log(var_export($err, true));
				$error_message = '<div class="hcap-wrap-error">Curl error. See error_log.</div>';
			}
			// no curl error
			else{
				// try to decode json from api
				$response = json_decode($response);
				// json_decode returns NULL if it can't decode
				if( $response === NULL ){
					$error_message = 'Could not decode response';
				}
				// got an error from the api?
				elseif( isset($response->error) ){
					$error_message = '<b>Error from Airtable API 👇</b>. Make sure you enter a valid key.<br/>' . $response->error->type . ' : ' . $response->error->message;
				}
				// no errors from curl or api
				else{
					$output .= '<div class="hcap-wrap-filters">';
					if( !$atts['disable_ui'] ){
						$output .= '<input autocomplete="off" placeholder="Search" type="search" name="hcap_search" class="hcap-wrap-filters-search">
							<div class="hcap-wrap-filters-toggle"><div class="hcap-wrap-filters-toggle-button">Filters <img class="inactive" src="' . HCAP_URL . '/dist/img/filter.svg"/><img class="active" src="' . HCAP_URL . '/dist/img/close.svg"/></div></div>
						%hcap_ui%';
					}
					$output .= '</div>
					<div class="hcap-wrap-tablecontainer">
					<table class="hcap-wrap-tablecontainer-table">';
					// get longest list of keys on a record
					$field_defaults = [];

					// loop through records to normalize the columns set for each row
					// this is because sometimes the airtable response doesn't provide keys for columns with empty values
					foreach( $response->records as $record ){
						// if the amount of fields of the record is more than the defaults
						if( count(get_object_vars($record->fields)) > count($field_defaults) ){
							// set the defaults
							$field_defaults = get_object_vars($record->fields);
						}
					}

					// set all defaults to their default
					foreach( $field_defaults as $key => $val ){
						$field_defaults[$key] = $table->fields[$key]['default_value'];
					}

					foreach( $response->records as $record_index => $record ){
						// merge defaults with fields
						$record->fields = (object) array_merge($field_defaults, get_object_vars($record->fields));
						// change column order based on table->fields registration
						$temp = array();
						foreach( $table->fields as $field_name => $field_value ){
							$temp[$field_name] = $record->fields->{$field_name};
						}
						$response->records[$record_index]->fields = (object) $temp;
					}

					// build dynamic fields
					if( !$atts['disable_ui'] ){
						$output = str_replace('%hcap_ui%', HCAP_FE::build_hcap_filter_ui($response, $table), $output);
					}

					// call before_records_loop hook
					$table->before_records_loop($response);

					// sort records based on default_sort
					$sort_data = array();
					// loop through the records to build the sort_data
					foreach( $response->records as $index => $record ) {
						// get the index of the sort field in the column order
						$sort_field_index = array_keys( array_filter( array_column($table->fields, 'default_sort') ) )[0];
						// get the name of the index
						$sort_field_name = array_keys(get_object_vars($record->fields))[$sort_field_index];
						// build the sort data
						// $record_index => column data anme
						$sort_data[$index] = $record->fields->{$sort_field_name};
					}

					// sort the data
					uasort($sort_data, function($a, $b) use ($table) {
						// use the custom sort callback if it's supplied
						if( $table->custom_table_sort($a, $b) !== '' ){
							return $table->custom_table_sort($a, $b);
						}
						// otherwise just sort alphabetically
						if( $a > $b ){
							return 1;
						}
						elseif( $a == $b ){
							return 0;
						}
						else{
							return -1;
						}
					});
					// build response->records from the sort data
					$new_records = [];
					foreach( $sort_data as $sort_index => $val ){
						$new_records[$sort_index] = $response->records[$sort_index];
					}
					$response->records = $new_records;

					// render the field data to the dom as json
					if( !empty($response) ){
						$output .= '<script type="text/javascript"> var ' . $shortcode . ' = ' . json_encode($response) . ';</script>';
					}

					// render the custom css
					if( !empty( $custom_table_css = $table->custom_table_css($table, $response) ) ){
						$output .= '<style type="text/css">';
						$output .= $custom_table_css;
						$output .= '</style>';
					}

					// render the custom js
					if( !empty( $custom_table_js = $table->custom_table_js($table, $response) ) ){
						$output .= '<script type="text/javascript">';
						$output .= $custom_table_js;
						$output .= '</script>';
					}

					// loop through the records
					$record_counter = 0;
					foreach($response->records as $index => $record){
						// get hidden columns
						$hidden_columns = array();
						foreach( $table->fields as $name => $data ){
							if( $data['hide_column'] ) $hidden_columns[] = $name;
						}

						// build header row
						if( $record_counter == 0 ){
							// start table row output
							$output .= '<tr data-sort-method="none">';
							foreach( $record->fields as $field_name => $field_value ){
								// don't make column headers for hidden columns
								if( !empty($hidden_columns) && in_array($field_name, $hidden_columns) ) continue;
								// build table header
								$output .= '<th>' . ucwords($field_name);
								// build the sort angle if the column is sortable
								// asc = alphabetical
								// desc = reverse alphabetical
								if( $table->fields[$field_name]['is_sortable'] ) $output .= '<span class="anglesort" data-direction="asc" data-column="' . $field_name . '">&blacktriangledown;</span>';
								// close the header tag
								$output .= '</th>';
							}
							$output .= '</tr>';
							// start table row output
							$output .= '<tr';
							if( !empty( $custom_row_class = $table->custom_row_class($index, $record) ) ){
								$output .= ' class="' . $custom_row_class . '"';
							}
							$output .= ' data-index="' . $index . '">';
						}
						else{
							// start table row output
							$output .= '<tr';
							if( !empty( $custom_row_class = $table->custom_row_class($index, $record) ) ){
								$output .= ' class="' . $custom_row_class . '"';
							}
							$output .= ' data-index="' . $index . '">';
						}
						// loop through fields & build table
						foreach($record->fields as $field_name => $field_value){
							// don't make columns for hidden columns
							if( !empty($hidden_columns) && in_array($field_name, $hidden_columns) ) continue;
							// build the rest of the table
							else{
								// if we're at a non-empty column that supports an array for the value
								// put each use case in a span
								if( gettype($field_value) == 'array' ){
									$output .= '<td data-column="' . strtolower(str_replace(' ', '_', $field_name)) . '">';
									foreach( $field_value as $item ){
										$output .= '<span class="multivalue">' . $item . '</span>';
									}
									$output .= '</td>';
								}
								// otherwise put the plain value in the table cell
								else{
									// use the field value unless its a bool value (produces a 1 if true) in which case use a checkmark (unicode)
									$output .= '<td data-column="' . strtolower(str_replace(' ', '_', $field_name)) . '">';
									$output .= !empty( $filtered_value = $table->filter_value($record, $field_name, $field_value) ) ? $filtered_value : $field_value;
									$output .= '</td>';
								}
							}
						}
						// end table row
						$output .= '</tr>';
						// bump the counter
						$record_counter++;
					}
					$output .= '</table></div>';
				}
			}
		}
		// key's empty
		else{
			$api_key = '';
			$error_message = '<div class="hcap-wrap-error">No api key found.</div>';
		}

		// dump out the error message
		if( $error_message ){
			$output .= $error_message;
		}

		// close table & wraps
		$output .= '</div>
				</div>';
		// poop out the html
		echo $output;

	}
}

HCAP_FE::init();

?>