// get the grunt class
const grunt = require('grunt');
// get the node sass implementation (cuz the ruby one is SLOW AF)
const sass = require('node-sass');
// auto-load all the grunt tasks passed into the initConfig func
require('load-grunt-tasks')(grunt);

// pass settings into grunt
grunt.initConfig({
    sass: {
        options: {
            implementation: sass,
            sourceMap: false,
        },
        dist: {
            files: {
                'dist/hcap-be.css': 'src/scss/backend.scss',
                'dist/hcap-fe.css': 'src/scss/frontend.scss',
            },
        }
    },
    cssmin: {
        options: {
            sourceMap: true,
        },
        dist: {
            files: {
                'dist/hcap-be.css': 'dist/hcap-be.css',
                'dist/hcap-fe.css': 'dist/hcap-fe.css'
            }
        }
    },
    autoprefixer: {
        options: {
            browsers: [
                "> 1%",
            ],
        },
        fe: {
            src: 'dist/hcap-fe.css',
            dest: 'dist/hcap-fe.css',
        },
        be:{
            src: 'dist/hcap-be.css',
            dest: 'dist/hcap-be.css',
        }
    },
    watch: {
        sass: {
            files: ['src/scss/**/*.scss'],
            tasks: ['sass', 'autoprefixer', 'cssmin'],
            options: {
                livereload: 35729
            },
        },
        js: {
            files: ['src/js/**/*.js'],
            tasks: ['browserify', 'exorcise'],
            options: {
                livereload: 35729
            },
        },
        php: {
            files: ['**/*.php'],
            options: {
                livereload: 35729
            },
        },
        options: {
            style: 'expanded',
            compass: true,
        },
    },
    browserify: {
        dist: {
            files: {
                'dist/hcap-fe.js': ['src/js/frontend.js'],
                'dist/hcap-be.js': ['src/js/backend.js'],
            },
            options: {
                transform: [
                    [
                        "babelify", {
                            presets: ["@babel/env"]
                        }
                    ]
                ],
                browserifyOptions : {
                    // Embed source map for tests
                    debug : true
                },
                sourceMaps: true
            }
        }
    },
    uglify: {
        dist: {
            files: {
                'dist/hcap-fe.js': ['dist/hcap-fe.js'],
                'dist/hcap-be.js': ['dist/hcap-be.js'],
            }
        }
    },
    exorcise: {
        bundle: {
            options: {},
            files: {
                'dist/hcap-fe.js.map': ['dist/hcap-fe.js'],
                'dist/hcap-be.js.map': ['dist/hcap-be.js'],
            }
        }
    }
});
// register the default task
grunt.registerTask('default', ['sass', 'autoprefixer', 'cssmin', 'browserify', 'exorcise', 'watch']);
// register the production task
grunt.registerTask('prod', ['sass', 'autoprefixer', 'cssmin', 'browserify', 'exorcise', 'uglify']);